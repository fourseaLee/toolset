import psutil

#获取进程名称
process_name = "python"

#检查进程是否存在
for process in psutil.process_iter():
    if process.name() == process_name:
        print("Process is still running.")
        break
else:
    print("Process has terminated.")

