    
def importData(csv_file:str, db_url: str, table:str):
    from pandas import read_csv
    df = read_csv(csv_file,index_col=0)
    from sqlalchemy import create_engine
    engine = create_engine(db_url, echo=True)
    sqlite_connection = engine.connect()
    df.to_sql(table, sqlite_connection, if_exists='replace', index=True, index_label="id")
    sqlite_connection.commit()
    sqlite_connection.close()

if __name__ == "__main__":
    importData("data/ticker.csv", "sqlite:///E:\workspace\crypto\crypto-rnn\\rnn.sqlite", "ticker")