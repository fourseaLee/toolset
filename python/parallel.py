from multiprocessing import Process,JoinableQueue
import time
q = JoinableQueue()
def caleTask(task, name):
    import hashlib
    print(task)
    content = task
    hash = hashlib.sha256()
    for i in range(10000000000):
      hash.update(content.encode(encoding="utf-8"))
      content =hash.hexdigest()
    return content

# 消费者方法
def consumer(q, task, name):
  while True:
    res = q.get()
    if res is None: 
      break
    print("%s 吃了 %s" % (name, res))
    hash = task(res, name)
    print(hash)
    q.task_done() # 发送信号给q.join(),表示已经从队列中取走一个值并处理完毕了
   
# 生产者方法
def producer(q, list_task, mod, index):
  for i in range(len(list_task)):
    if i % mod != index:
      continue
    time.sleep(1) # 模拟生产西瓜的时间延迟
    res = "task %s" % (list_task[i])

    print("%d 生产了 %s" % (index, list_task[i]))
    # 把生产的vegetable放入到队列中
    q.put(res)
  q.join() # 等消费者把自己放入队列的所有元素取完之后才结束
 
list_task = ["BTC", "ETH", "BNB", "SOL", "XRP", "FIL", "ADA", "DOGE", "EOS", "CKB"] 
if __name__ == "__main__":
  # q = Queue()
  
  # 创建生产者
  p1 = Process(target=producer, args=(q, list_task, 2, 0))
  p2 = Process(target=producer, args=(q, list_task, 2, 1))
  # 创建消费者
  c1 = Process(target=consumer, args=(q, caleTask,"peter",))
  c2 = Process(target=consumer, args=(q, caleTask,"peter2",))
  c3 = Process(target=consumer, args=(q, caleTask,"peter3",))
 
  c1.daemon = True
  c2.daemon = True
  c3.daemon = True
 
  p_l = [p1, p2, c1, c2, c3]
  for p in p_l:
    p.start()
  
  p1.join()
  p2.join()
  # 1.主进程等待p1,p2进程结束才继续执行
  # 2.由于q.join()的存在,生产者只有等队列中的元素被消费完才会结束
  # 3.生产者结束了,就代表消费者已经消费完了,也可以结束了,所以可以把消费者设置为守护进程(随着主进程的退出而退出)

  print("主进程")