import pandas  as pd 
df = pd.read_feather("./data/BTC_USDT-1s.feather")
df["open time"] = df.date.astype("int64")/1000000
df.to_csv("data/BTC_USDT-1s.csv", index=False)

import json
df = pd.read_csv("./data/BTCUSDT-depth-2024-10-15.csv")
bids_list = []
asks_list = []
def format(depths:list):
    fmt_depths = []
    for depth in depths:
        dict_depths = {}
        dict_depths["price"] = depth[0] 
        dict_depths["amount"] = depth[1]
        fmt_depths.append(dict_depths)
    return fmt_depths


for idx, value in df.iterrows():
    #bids = value["bids"]
    #asks = value["asks"]
    bids = json.loads(value["bids"])
    fmt_bids = format(json.loads(value["bids"]))
    fmt_asks = format(json.loads(value["asks"]))
    #print(json.dumps(fmt_bids))
    #print(json.dumps(fmt_asks))
    bids_list.append(json.dumps(fmt_bids))
    asks_list.append(json.dumps(fmt_asks))
    
df["bids"] = bids_list
df["asks"] = asks_list
df["symbol"] = "BTC_USDT"
df.to_csv("ceshi.csv", index=False)