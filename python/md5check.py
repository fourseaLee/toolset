import hashlib
import os
dir1="E:\workspace" 
dir2="D:\workspace" 
result = []
def file_name(file_dir): 
    path_file={}
    for root, dirs, files in os.walk(file_dir):
        for i in files:
            with open((root+'\\'+i), "rb") as f:
                file_hash = hashlib.md5()
                while chunk := f.read(8192):
                        file_hash.update(chunk)
                        get_name=root+'\\'+i
                        path_file[get_name]=file_hash.hexdigest()
    return(path_file)
                        #print(get_name,file_hash.hexdigest())
def match(dict1,dict2):
    for dict1_key,dict1_vlues in dict1.items():
        for dict2_key,dict2_vlues in dict2.items():
            if dict1_vlues==dict2_vlues:
                dict_value = {"sdir":dict1_key, "shash":dict1_vlues, "ddir":dict2_key, "dhash":dict2_vlues}
                result.append(dict_value)
                #print(dict1_key,dict1_vlues,dict2_key,dict2_vlues)
match(file_name(dir1),file_name(dir2))
import pandas as pd
df_md5 = pd.DataFrame(result)
df_md5.to_csv("./data/md5samefile.csv")