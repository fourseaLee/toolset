import subprocess
import platform
import os
import json
import argparse

current_path = os.getcwd();
def getChDir(path="D:\\workspace\\rust"):
    os.chdir(path)
    ret = os.listdir(os.getcwd())
    print(ret)
    return ret

def bakGit():
    cmd_string = f"git remote get-url origin"
    git_url = ""
    if platform.system() == "Linux":
        cmd_return = subprocess.run(["git", "remote", "get-url","origin"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        git_url = cmd_return.stdout.replace("\n", "")
    else:
        cmd_return = subprocess.run(cmd_string, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        git_url = cmd_return.stdout
    return git_url

'''path = "/home/foursea/github"
list_dir = getChDir(path)
git_back = {}
file_back = {}
project_back = {}
for chdir in list_dir:
    
    bak_path = f"{path}/{chdir}"
    if os.path.isdir(bak_path):
        print(bak_path)
        os.chdir(bak_path)
        git_url = bakGit()
        #if git_url == "":
        #    project_back[chdir] = True
        #else:
        #    git_back[chdir] = git_url
        git_back[chdir] = git_url
    else:
        file_back[chdir] = True
back_dict = {"git":git_back, "file":file_back}
os.chdir(current_path)
with open("backup.json", "w") as fp:
    json.dump(back_dict, fp, indent=2)
back_dict = json.dumps(back_dict, indent=2)
bakGit()
'''
def backup(path, name):
    list_dir = getChDir(path)
    git_back = {}
    file_back = {}
    path_back = {}
    for chdir in list_dir:
        
        bak_path = f"{path}{chdir}"
        if os.path.isdir(bak_path):
            print(bak_path)
            os.chdir(bak_path)
            git_url = bakGit()
            if git_url == "":
                path_back[chdir] = True
            else:
                git_back[chdir] = git_url
            #git_back[chdir] = git_url
        else:
            file_back[chdir] = True
    back_dict = {"git":git_back, "file":file_back, "only_path": path_back}
    os.chdir(current_path)
    with open(f"{name}_backup.json", "w") as fp:
        json.dump(back_dict, fp, indent=2)
    back_dict = json.dumps(back_dict, indent=2)    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--version', '-v', action='version',
                        version='%(prog)s version : v 0.01', help='show the version')

    parser.add_argument('--path', '-p', help='back up path ')

    parser.add_argument('--name', '-n', help='back up name ')

    args = parser.parse_args()
    path = args.path
    name = args.name
    backup(path, name)
    
    print("=== end ===")

