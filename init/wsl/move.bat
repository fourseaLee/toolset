%查看基本信息%
wsl --list --verbose
%关闭正在运行的子系统%
wsl --shutdown
%导出子系统进行备份%
wsl --export Ubuntu-22.04 D:\WSL\UbuntuBackup.tar
%移除原有的子系统%
wsl --unregister Ubuntu-22.04
%导入备份好的的子系统%
wsl --import Ubuntu-22.04 e:\WSL\Ubuntu22 e:\WSL\UbuntuBackup.tar --version 2
%启动子系统%
wsl -d Ubuntu-22.04
